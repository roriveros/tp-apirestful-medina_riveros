**Trabajo Practico Web Services**

**Integrantes**
* Manuel Medina
* Romina Riveros

**Elementos a utilizar**
* JavaEE, JDK 1.8
* IDE Eclipse, elegir la opción Eclipse for Java EE
* IDE Eclipse, instalar Jboss Tools (Menú: Help -> Eclipse MarketPlace. Buscar "Jboss Tools"). Este paso solo es necesario para crear un proyecto nuevo.
* Instalar Jboss tools 
* Instalar SOAP-UI
* Instalar PostMan para Google Chrome
* Instalar el motor de base de datos Postgresql

**Descargar el proyecto**

Via SSH:
    
    git clone git@gitlab.com:roriveros/tp-apirestful-medina_riveros.git
    
    
Via HTTPS:
    
    git clone https://gitlab.com/roriveros/tp-apirestful-medina_riveros.git
    
Via transferencia de archivos por parte del Profesor

    
**Importar proyecto**
 * Abrir el IDE Eclipse
 * Menú File -> Import -> Existing Maven Project
 * Dentro de los archivos descargados, especificar la carpeta "sd\lab-ws\servidor\personas"
 * Finalizar
 * El IDE comenzará a importar las librerías de Maven
 
Base de datos

Crear una base de datos.
Configurar los datos de configuración y acceso en la clase "Bd.java".
```sql
CREATE TABLE persona
(
    cedula integer NOT NULL,
    nombre character varying(100),
    apellido character varying(100),
    CONSTRAINT pk_persona PRIMARY KEY (cedula)
)
WITH (
   OIDS=FALSE
);
```
```sql
CREATE TABLE asignatura
(
  codigo integer NOT NULL,
  nombre character varying(100),
  departamento character varying(100),
  CONSTRAINT pk_asignatura PRIMARY KEY (codigo)
)
WITH (
  OIDS=FALSE
);
```
```sql 
CREATE TABLE persona_asignatura
(
  id integer NOT NULL DEFAULT nextval('persona_asignatura_id_seq'::regclass),
  cod_persona integer NOT NULL,
  cod_asignatura integer NOT NULL,
  CONSTRAINT pk_personaasignatura PRIMARY KEY (id),
  CONSTRAINT fk_asignatura FOREIGN KEY (cod_asignatura)
      REFERENCES public.asignatura (codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_persona FOREIGN KEY (cod_persona)
      REFERENCES public.persona (cedula) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
```

```sql
CREATE SEQUENCE public.persona_asignatura_id_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE public.persona_asignatura_id_seq
OWNER TO postgres;
```

**Deployar en Servidor**

* Desde el IDE Eclipse, configurar el servidor de aplicaciones Wildfly10
* Despliegue del proyecto "personas" en el servidor Wildfly

--------->>>>>>>URL para los servicios RestFul

--->>>Servicios RestFull de ASIGNATURAS

- PONGA http://localhost:8080/personas/rest/asignaturas
- GET http://localhost:8080/personas/rest/asignaturas
- ELIMINAR http://localhost:8080/personas/rest/asignaturas/2
- GET http://localhost:8080/personas/rest/asignaturas/asociar?Codigo=2&cedula=4604126
- GET http://localhost:8080/personas/rest/asignaturas/desasociar?Codigo=2&cedula=4604126
- GET http://localhost:8080/personas/rest/asignaturas/lista_asignaturas?Cedula=4604126
- GET http://localhost:8080/personas/rest/asignaturas/lista_personas?Codigo=2


--->>>Servicios Restfull de CLIENTE

- GET http://localhost:8080/personas/rest/cliente/lista_asignaturas
- GET http://localhost:8080/personas/rest/cliente/lista_personas
- POST http://localhost:8080/personas/rest/cliente/crearpersona
- POST http://localhost:8080/personas/rest/cliente/crearasignatura


