package py.una.pol.personas.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.PersonaAsignatura;

@Stateless
public class AsignaturaDAO {


    @Inject
    private Logger log;

	/**
	 *
	 * @param condiciones
	 * @return
	 */
	public List<Asignatura> seleccionar() {
		String query = "SELECT codigo, nombre, departamento FROM asignatura ";

		List<Asignatura> lista = new ArrayList<Asignatura>();

		Connection conn = null;
        try {
        	conn = Bd.connect();
        	ResultSet rs = conn.createStatement().executeQuery(query);

        	while(rs.next()) {
        		Asignatura a = new Asignatura();
        		a.setCodigo(rs.getLong(1));
        		a.setNombre(rs.getString(2));
        		a.setDepartamento(rs.getString(3));

        		lista.add(a);
        	}

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
	}

	public Asignatura seleccionarPorCodigo(long codigo) {
		String query = "SELECT codigo, nombre, departamento FROM asignatura WHERE codigo = ? ";

		Asignatura a = null;

		Connection conn = null;
        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, codigo);

        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		a = new Asignatura();
        		a.setCodigo(rs.getLong(1));
        		a.setNombre(rs.getString(2));
        		a.setDepartamento(rs.getString(3));
        	}

        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return a;
	}


    public long insertar(Asignatura a) throws SQLException {

        String query = "INSERT INTO asignatura(codigo, nombre, departamento) "
                + "VALUES(?,?,?)";

        long id = 0;
        Connection conn = null;

        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, a.getCodigo());
            pstmt.setString(2, a.getNombre());
            pstmt.setString(3, a.getDepartamento());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }

        return id;
    }  
    
    public long actualizar(Asignatura a) throws SQLException {

        String query = "UPDATE asignatura SET nombre = ? , departamento = ? WHERE codigo = ? ";

        long id = 0;
        Connection conn = null;

        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, a.getNombre());
            pstmt.setString(2, a.getDepartamento());
            pstmt.setLong(3, a.getCodigo());

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la actualizacion: " + ex.getMessage());
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
        return id;
    }

    public long borrar(long codigo) throws SQLException {

        String query = "DELETE FROM asignatura WHERE codigo = ? ";

        long id = 0;
        Connection conn = null;

        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1, codigo);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }

    /*
     * Crea una asociacion de una persona y una asignatura.
     */
    public long asociar(long codigo, long cedula) throws SQLException {
        String query = "INSERT INTO persona_asignatura(cod_persona, cod_asignatura) "
                + "VALUES(?, ?)";

        long id = 0;
        Connection conn = null;

        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, codigo);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	throw ex;
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }

        return id;
    }

    /*
     * Eliminar una asociacion de una persona a una asignatura.
     */
    public long desasociar(long codigo, long cedula) throws SQLException {
        String query = "DELETE FROM persona_asignatura WHERE cod_persona = ? and cod_asignatura = ? ";

        long id = 0;
        Connection conn = null;

        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
            pstmt.setLong(1, cedula);
            pstmt.setLong(2, codigo);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getLong(1);
                    }
                } catch (SQLException ex) {
                	log.severe("Error en la eliminación: " + ex.getMessage());
                	throw ex;
                }
            }
        } catch (SQLException ex) {
        	log.severe("Error en la eliminación: " + ex.getMessage());
        	throw ex;
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        		throw ef;
        	}
        }
        return id;
    }
    
    /*
     * Buscar si una persona ya tiene asociado una asignatura.
     */
    public PersonaAsignatura seleccionarCogidoCedula(long codigo, long cedula) {
    	String query = "SELECT id, cod_persona, cod_asignatura FROM persona_asignatura WHERE cod_asignatura = ? and cod_persona = ? ";

		PersonaAsignatura pa = null;

		Connection conn = null;
        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, codigo);
        	pstmt.setLong(2, cedula);

        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		pa = new PersonaAsignatura();
        		pa.setId(rs.getLong(1));
        		pa.setCod_persona(rs.getLong(2));
        		pa.setCod_asignatura(rs.getLong(3));
        	}

        } catch (SQLException ex) {
        	log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return pa;
	}

    /*
     * Lista todas las asignaturas de una persona.
     */
    public List<Asignatura> listarAsignaturaPersona(long cedula) {
    	String query = "SELECT a.codigo, a.nombre, a.departamento FROM asignatura a " +
    	"INNER JOIN persona_asignatura pa ON pa.cod_asignatura = a.codigo " +
    	"INNER JOIN persona p ON p.cedula = pa.cod_persona " +
    	"WHERE p.cedula = ?";

		List<Asignatura> lista = new ArrayList<Asignatura>();

		Connection conn = null;
        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, cedula);
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Asignatura a = new Asignatura();
        		a.setCodigo(rs.getLong(1));
        		a.setNombre(rs.getString(2));
        		a.setDepartamento(rs.getString(3));

        		lista.add(a);
        	}

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
    }

    /*
     * Lista todas las personas de una asignatura.
     */
    public List<Persona> listarPersonaAsignatura(long codigo) {
    	String query = "SELECT p.cedula, p.nombre, p.apellido FROM persona p " +
    	"INNER JOIN persona_asignatura pa ON pa.cod_persona = p.cedula " +
    	"INNER JOIN asignatura a ON a.codigo = pa.cod_asignatura " +
    	"WHERE a.codigo = ? ";

		List<Persona> lista = new ArrayList<Persona>();

		Connection conn = null;
        try {
        	conn = Bd.connect();
        	PreparedStatement pstmt = conn.prepareStatement(query);
        	pstmt.setLong(1, codigo);
        	ResultSet rs = pstmt.executeQuery();

        	while(rs.next()) {
        		Persona p = new Persona();
        		p.setCedula(rs.getLong(1));
        		p.setNombre(rs.getString(2));
        		p.setApellido(rs.getString(3));

        		lista.add(p);
        	}

        } catch (SQLException ex) {
            log.severe("Error en la seleccion: " + ex.getMessage());
        }
        finally  {
        	try {
        		conn.close();
        	}catch(Exception ef){
        		log.severe("No se pudo cerrar la conexion a BD: "+ ef.getMessage());
        	}
        }
		return lista;
    }

}
