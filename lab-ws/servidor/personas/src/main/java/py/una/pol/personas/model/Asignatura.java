package py.una.pol.personas.model;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@XmlRootElement
public class Asignatura implements Serializable {

	private Long codigo;
	private String nombre;
	private String departamento;
	
	public Asignatura() {
		
	}
	
	public Asignatura(Long codigo, String nombre, String departamento) {
		this.codigo = codigo;
		this.nombre = nombre;
		this.departamento = departamento;
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
}
