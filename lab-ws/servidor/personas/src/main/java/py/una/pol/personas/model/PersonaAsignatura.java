package py.una.pol.personas.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@XmlRootElement
public class PersonaAsignatura implements Serializable {
	
	private Long id;
	private Long cod_persona;
	private Long cod_asignatura;
	
	public PersonaAsignatura() {
		
	}

	public PersonaAsignatura(Long id, Long cod_persona, Long cod_asignatura) {
		this.id = id;
		this.cod_persona = cod_persona;
		this.cod_asignatura = cod_asignatura;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCod_persona() {
		return cod_persona;
	}

	public void setCod_persona(Long cod_persona) {
		this.cod_persona = cod_persona;
	}

	public Long getCod_asignatura() {
		return cod_asignatura;
	}

	public void setCod_asignatura(Long cod_asignatura) {
		this.cod_asignatura = cod_asignatura;
	}
	
}
