package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaService;
import py.una.pol.personas.service.PersonaService;

/**
 * JAX-RS Example
 * <p/>
 * Esta clase produce un servicio RESTful para leer y escribir contenido de
 * asignaturas
 */
@Path("/asignaturas")
@RequestScoped
public class AsignaturaRESTService {

	@Inject
	private Logger log;

	@Inject
	AsignaturaService asignaturaService;

	@Inject
	PersonaService personaService;

	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response listar() {
		List<Asignatura> lista;
		Response.ResponseBuilder builder = null;
		
		try {
			lista = asignaturaService.seleccionar();
			builder = Response.status(201).entity(lista);
			log.info("Lista las asignaturas");
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
			log.warning("Error: " + responseObj);
		}
		return builder.build();
	}

	@GET
	@Path("/{codigo:[0-9][0-9]*}")
	@Produces(MediaType.APPLICATION_JSON)
	public Asignatura obtenerPorId(@PathParam("codigo") long codigo) {
		Asignatura a = asignaturaService.seleccionarPorCodigo(codigo);
		if (a == null) {
			log.info("obtenerPorId " + codigo + " no encontrado.");
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
		log.info("obtenerPorId " + codigo + " encontrada: " + a.getNombre());
		return a;
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crear(Asignatura a) {

		Response.ResponseBuilder builder = null;

		try {
			asignaturaService.crear(a);
			builder = Response.status(201).entity("Asignatura creada exitosamente");

		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response modificar(Asignatura a) {

		Response.ResponseBuilder builder = null;

		try {
			if (asignaturaService.seleccionarPorCodigo(a.getCodigo()) == null) {
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
			} else {
				asignaturaService.modificar(a);
				builder = Response.status(201).entity("Asignatura modificado exitosamente");
			}		
		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builder.build();
	}
	
	@DELETE
	@Path("/{codigo}")
	public Response borrar(@PathParam("codigo") long codigo) {
		Response.ResponseBuilder builder = null;
		try {

			if (asignaturaService.seleccionarPorCodigo(codigo) == null) {
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
			} else {
				asignaturaService.borrar(codigo);
				builder = Response.status(202).entity("Asignatura borrada exitosamente.");
			}

		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}
		return builder.build();
	}

	@GET
	@Path("/asociar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response asociar(@QueryParam("codigo") long codigo, @QueryParam("cedula") long cedula) {
		Response.ResponseBuilder builder = null;

		try {
			if (asignaturaService.seleccionarPorCodigo(codigo) == null && personaService.seleccionarPorCedula(cedula) == null) {
				log.info("Asignatura: " + codigo + ", Persona: " + cedula + ", no existen." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura y Persona no existen.");
			} else if (asignaturaService.seleccionarPorCodigo(codigo) == null) {
				log.info("Asignatura: " + codigo + ", no existe." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
			} else if (personaService.seleccionarPorCedula(cedula) == null) {
				log.info("Persona: " + cedula + ", no existe." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
			} else if (asignaturaService.seleccionarCodigoCedula(codigo, cedula) != null) {
				log.info("Asignatura: " + codigo + ", Persona: " + cedula + ", ya existe esta asociacion." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Ya existe esta asociacion.");
			} else {
				asignaturaService.asociar(codigo, cedula);
				builder = Response.status(201).entity("Asociacion exitosamente");
				log.info("Asociacion exitosamente");
			}
		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
			log.warning("Error-DB: " + responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
			log.warning("Error: " + responseObj);
		}

		return builder.build();
	}

	@GET
	@Path("/desasociar")
	@Produces(MediaType.APPLICATION_JSON)
	public Response desasociar(@QueryParam("codigo") long codigo, @QueryParam("cedula") long cedula) {
		Response.ResponseBuilder builder = null;

		try {
			if (asignaturaService.seleccionarPorCodigo(codigo) == null && personaService.seleccionarPorCedula(cedula) == null) {
				log.info("Asignatura: " + codigo + ", Persona: " + cedula + ", no existen." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura y Persona no existen.");
			} else if (asignaturaService.seleccionarPorCodigo(codigo) == null) {
				log.info("Asignatura: " + codigo + ", no existe." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura no existe.");
			} else if (personaService.seleccionarPorCedula(cedula) == null) {
				log.info("Persona: " + cedula + ", no existe." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona no existe.");
			} else if (asignaturaService.seleccionarCodigoCedula(codigo, cedula) == null) {
				log.info("Asignatura: " + codigo + ", Persona: " + cedula + ", no existe esta asociacion." );
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("No existe esta asociacion.");
			} else {
				asignaturaService.desasociar(codigo, cedula);
				builder = Response.status(202).entity("Desasociacion exitosamente");
				log.info("Desasociacion exitosamente");
			}
		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
			log.warning("Error-DB: " + responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
			log.warning("Error: " + responseObj);
		}

		return builder.build();
	}
	
	@GET
	@Path("/lista_asignaturas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarAsignaturasPersona(@QueryParam("cedula") long cedula) {
		List<Asignatura> lista;
		Response.ResponseBuilder builder = null;
		
		try {
			if(personaService.seleccionarPorCedula(cedula) == null) {
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona con cedula " + cedula + ", no existe.");
				log.info("Persona con cedula " + cedula + ", no existe.");
			} else {
				lista = asignaturaService.listarAsignaturaPersona(cedula);
				builder = Response.status(201).entity(lista);
				log.info("Lista asignatura de un persona con cedula " + cedula);
			}
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
			log.warning("Error: " + responseObj);
		}
		return builder.build();
	}
	
	@GET
	@Path("/lista_personas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPersonasAsignatura(@QueryParam("codigo") long codigo) {
		List<Persona> lista;
		Response.ResponseBuilder builder = null;
		
		try {
			if(asignaturaService.seleccionarPorCodigo(codigo) == null) {
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Asignatura con codigo " + codigo + ", no existe.");
				log.info("Asignatura con codigo " + codigo + ", no existe.");
			} else {
				lista = asignaturaService.listarPersonaAsignatura(codigo);
				builder = Response.status(201).entity(lista);
				log.info("Lista personas de un asignatura con codigo " + codigo);
			}
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
			log.warning("Error: " + responseObj);
		}
		return builder.build();
	}
}
