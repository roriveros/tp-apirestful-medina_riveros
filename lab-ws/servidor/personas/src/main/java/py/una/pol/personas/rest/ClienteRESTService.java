package py.una.pol.personas.rest;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.service.AsignaturaService;
import py.una.pol.personas.service.PersonaService;


/**
 * Integrantes del Grupo
 * Romina Riveros
 * Manuel Medina
 */
@Path("/cliente")
@RequestScoped
public class ClienteRESTService {
	
	@Inject
	private Logger log;

	@Inject
	AsignaturaService asignaturaService;

	@Inject
	PersonaService personaService;
	
	@GET
	@Path("/lista_personas")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public List<Persona> listar() {
        return personaService.seleccionar();
    }
    
    
    @GET
	@Path("/lista_asignaturas")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarAsignaturasPersona(@QueryParam("cedula") long cedula) {
		List<Asignatura> lista;
		Response.ResponseBuilder builder = null;
		
		try {
			if(personaService.seleccionarPorCedula(cedula) == null) {
				builder = Response.status(Response.Status.NOT_ACCEPTABLE).entity("Persona con cedula " + cedula + ", no existe.");
				log.info("Persona con cedula " + cedula + ", no existe.");
			} else {
				lista = asignaturaService.listarAsignaturaPersona(cedula);
				builder = Response.status(201).entity(lista);
				log.info("Lista asignatura de un persona con cedula " + cedula);
			}
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
			log.warning("Error: " + responseObj);
		}
		return builder.build();
	}

	
	
	/**
     * Creates a new member from the values provided. Performs validation, and will return a JAX-RS response with either 200 ok,
     * or with a map of fields, and related errors.
     */
    @POST
    @Path("/crearpersona")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearpersona(Persona p) {

        Response.ResponseBuilder builder = null;

        try {
            personaService.crear(p);
            builder = Response.status(201).entity("Persona creada exitosamente");
            
        } catch (SQLException e) {
            // Handle the unique constrain violation
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("bd-error", e.getLocalizedMessage());
            builder = Response.status(Response.Status.CONFLICT).entity(responseObj);
        } catch (Exception e) {
            // Handle generic exceptions
            Map<String, String> responseObj = new HashMap<>();
            responseObj.put("error", e.getMessage());
            builder = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
        }

        return builder.build();
    }
    
    @POST
    @Path("/crearasignatura")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response crearasignatura(Asignatura a) {

		Response.ResponseBuilder builde = null;

		try {
			asignaturaService.crear(a);
			builde = Response.status(201).entity("Asignatura creada exitosamente");

		} catch (SQLException e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("bd-error", e.getLocalizedMessage());
			builde = Response.status(Response.Status.CONFLICT).entity(responseObj);
		} catch (Exception e) {
			Map<String, String> responseObj = new HashMap<>();
			responseObj.put("error", e.getMessage());
			builde = Response.status(Response.Status.BAD_REQUEST).entity(responseObj);
		}

		return builde.build();
	}
    
    

}
