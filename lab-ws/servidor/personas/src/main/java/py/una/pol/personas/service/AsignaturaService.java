package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.AsignaturaDAO;
import py.una.pol.personas.model.Asignatura;
import py.una.pol.personas.model.Persona;
import py.una.pol.personas.model.PersonaAsignatura;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class AsignaturaService {

    @Inject
    private Logger log;

    @Inject
    private AsignaturaDAO dao;

    public void crear(Asignatura a) throws Exception {
        log.info("Creando Asignatura: " + a.getNombre());
        try {
        	dao.insertar(a);
        }catch(Exception e) {
        	log.severe("ERROR al crear asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Asignatura creada con éxito: " + a.getNombre());
    }
    
    public List<Asignatura> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Asignatura seleccionarPorCodigo(long codigo) {
    	return dao.seleccionarPorCodigo(codigo);
    }
    
    public void modificar(Asignatura a) throws Exception {
        log.info("Modificando Asignatura: " + a.getNombre());
        try {
        	dao.actualizar(a);
        }catch(Exception e) {
        	log.severe("ERROR al modificar la asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Asignatura modificado con éxito: " + a.getNombre());
    }
    
    public long borrar(long codigo) throws Exception {
    	return dao.borrar(codigo);
    }
    
    public void asociar(long codigo, long cedula) throws Exception {
        log.info("Asociando: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
        try {
        	dao.asociar(codigo, cedula);
        }catch(Exception e) {
        	log.severe("ERROR al asociar persona con asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Asociacion con éxito: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
    }
    
    public void desasociar(long codigo, long cedula) throws Exception {
        log.info("Desasociando: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
        try {
        	dao.desasociar(codigo, cedula);
        }catch(Exception e) {
        	log.severe("ERROR al desasociar persona con asignatura: " + e.getLocalizedMessage());
        	throw e;
        }
        log.info("Desasociacion con éxito: Codigo de asignatura: " + codigo + " Cedula persona: " + cedula);
    }
    
    public PersonaAsignatura seleccionarCodigoCedula(long codigo, long cedula) {
    	return dao.seleccionarCogidoCedula(codigo, cedula);
    }
    
    public List<Asignatura> listarAsignaturaPersona(long cedula) {
    	return dao.listarAsignaturaPersona(cedula);
    }
    
    public List<Persona> listarPersonaAsignatura(long codigo) {
    	return dao.listarPersonaAsignatura(codigo);
    }
}
