package py.una.pol.personas.service;


import javax.ejb.Stateless;
import javax.inject.Inject;

import py.una.pol.personas.dao.PersonaDAO;
import py.una.pol.personas.model.Persona;

import java.util.List;
import java.util.logging.Logger;

// The @Stateless annotation eliminates the need for manual transaction demarcation
@Stateless
public class PersonaService {

    @Inject
    private Logger log;

    @Inject
    private PersonaDAO dao;

    public void crear(Persona p) throws Exception {
        log.info("Creando Persona: " + p.getNombre() + " " + p.getApellido());
        try {
        	dao.insertar(p);
        }catch(Exception e) {
        	log.severe("ERROR al crear persona: " + e.getLocalizedMessage() );
        	throw e;
        }
        log.info("Persona creada con éxito: " + p.getNombre() + " " + p.getApellido() );
    }
    
    public List<Persona> seleccionar() {
    	return dao.seleccionar();
    }
    
    public Persona seleccionarPorCedula(long cedula) {
    	return dao.seleccionarPorCedula(cedula);
    }
    
    public long borrar(long cedula) throws Exception {
    	return dao.borrar(cedula);
    }
}
